# KUtils Plugins
This repository is what is hooked into the `fetchPlugin` on [KUtils](https://gitlab.com/Aberrantfox/KUtils)

### What are plugins?
Plugins are made with idiomatic kotlin. Now, you can easily use the built in functionality
in kutils to just load up your own local plugins, but if you want to be able to do the following:

```kotlin
startBot(...) {
    fetchPlugin("MyAwesomePlugin")
}
```

you need to contribute it here. 


### How do I write a plugin?
If you want to write a plugin, you can take a look at what is currently available, but 
here is a quick step by step guide:

1. First, clone this repository
2. Create a file in the resources directory called <YOUR_PLUGIN_NAME>.kts - it must end with .kts
3. Write the plugin. Don't worry, I won't leave you there with no further information, here's
   what you need to know:
   
   **Variables you have access to**
   At any point in your plugin, you have access to the following two variables:
   
   - `container` - A CommandsContainer.
   - `kutils` - A hook to the KUtils API. 
   
   You can leverage these variables to create commands, use the JDA api, etc.
4. Note that at any point you can `import` something at the top of your file. If you do require
   external libraries, please note that these will need to be on the classpath at runtime.
5. Once you have your plugin all made up and tested, you can submit an MR here. Please note, 
   in your merge request you should include/do all of the following:
      - Links to all dependencies that aren't KUtils, Kotlin, or the standard library.
      - A brief description of the plugin
      - The description field filled out for all commands
      - Ensure that the file you are committing ends with .kts and doesn't have the word `plugin` in it,
        e.g. PingPlugin.kts is just unnecessarily long, just name it Ping.kts
6. And you're done. With a bit of luck, it'll be accepted :) 


### Examples
You can find examples in the resources directory, found [here](https://gitlab.com/Aberrantfox/KUtils-Plugins/tree/master/src/main/resources)