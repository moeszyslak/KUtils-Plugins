container.command("ping") {
    execute {
        it.respond("pong!")
    }
}