package me.aberrantfox.kutilsplugins

import org.reflections.Reflections
import org.reflections.scanners.ResourcesScanner
import org.reflections.util.ClasspathHelper
import org.reflections.util.ConfigurationBuilder
import org.reflections.util.FilterBuilder


class PluginLoader {
    private val pluginMap = HashMap<String, String>()

    init {
        val filter = FilterBuilder().include(".*\\.kts")
        val reflections = Reflections(ConfigurationBuilder()
                .filterInputsBy(filter)
                .setScanners(ResourcesScanner())
                .setUrls(listOf(ClasspathHelper.forClass(PluginLoader::class.java))))

        reflections.getResources { true }.forEach {
            pluginMap[it.removeSuffix(".kts").toLowerCase()] =
                    PluginLoader::class.java.getResource("/$it").readText()
        }
    }

    operator fun get(name: String) = pluginMap[name]
}

fun main(args: Array<String>) {
    PluginLoader()
}